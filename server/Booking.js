const express = require('express')
const bodyParser = require('body-parser')

const route = express.Router();
const mongoose = require("mongoose");
require('../modal/busRoute.modal')
const BusRoute = mongoose.model('busroute');
const ticketBooking = mongoose.model('ticketBooking')

var JSONData = require('./datas.json')

route.post('/search', (req, res) => {
    var sData = req.body;
    console.log(sData)
    searchValidation(sData, (errorMsg) => {
        if (errorMsg == '') {
            let SortBy = sData.SortBy != '' ? sData.SortBy : 'Price',
                SortOrder = sData.SortOrder == 'asc' ? 1 : -1 || 1;

            var searchOption = {
                SCity: sData.SCity,
                DCity: sData.DCity
            }

            BusRoute.find(searchOption, (err, data) => {
                if (err) return console.log(err)
                // BusRoute.countDocuments(searchOption, function (err, count) { })
                res.status(200).json({ messageType: "Success", SearchedData: data })
            }).sort({ "Price": SortOrder }).countDocuments()

        }
        else {
            res.status(200).json({ messageType: "Error", message: errorMsg })
        }
    })
})

route.post('/book', (req, res) => {
    var bData = req.body;
    var BookTicket = new ticketBooking();
    BookTicket.BusNumber = bData.BusNumber;
    BookTicket.SeatNumber = bData.SeatNumber;
    BookTicket.UserName = bData.UserName;
    BookTicket.JournyDate = bData.JournyDate;
    var BookOption = {
        BusNumber: bData.BusNumber,
        SeatNumber: bData.SeatNumber,
        JournyDate: bData.JournyDate
    }
    ticketBooking.countDocuments(BookOption, function (err, count) {
        if (count == 0) {
            BookTicket.save((err, data) => {
                if (err) {
                    return res.status(200).json({ messageType: "Error", message: err.message })
                }
                console.log(data)
                res.status(200).json({ messageType: "Success", message: data })
            })
        }
        else {
            return res.status(200).json({ messageType: "Error", message: `Seat number ${BookOption.SeatNumber}  aldreay occupied in ${BookOption.BusNumber} bus on ${BookOption.JournyDate}` })

        }
    })

})

function searchValidation(searchedData, callback) {
    var errorMsg = ''
    if (searchedData.SCity == '')
        errorMsg += 'Source City is required \n'
    if (searchedData.DCity == '')
        errorMsg += 'Destination City is required \n'
    if (searchedData.SDate == '')
        errorMsg += 'Start Date is required'
    setTimeout(() => {
        callback(errorMsg)
    }, 100);
};

module.exports = route