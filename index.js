const connection = require("./modal");
const express = require('express')
const bodyParser = require('body-parser')

const app = express()

const Booking = require('./server/Booking')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', function (req, res) {
  res.status(200).json({ message: 'Hello World' })
})

app.use('/Booking', Booking)

app.listen(3000)