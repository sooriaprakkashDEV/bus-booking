# altimetrik
Task 
Bus Booking Sytem

### Tech

It uses a number of open source projects to work properly:

* [VS Code] - awesome web-based text editor
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend
* [Express] - fast node.js web app framework
* [jQuery] - The Write Less, Do More, JavaScript Library.


### Installation

Install the dependencies and start the server.

```sh
$ cd BUS
$ npm install
$ node index
```

Verify the development by navigating to your server address in your preferred browser.

```sh
127.0.0.1:3001
```

Some Routes are
```sh
For searching for a bus
POST http://localhost:3000/Booking/search

Input = {
  SCity: 'mdu',       
  DCity: 'cbe',       
  SDate: '21-12-2019',
  RDate: '',
  SortBy: '',
  SortOrder: ''       
}

http://localhost:3000/Booking/book

Input = {
    [{"key":"BusNumber","value":"TN10C0001","description":""},{"key":"SeatNumber","value":"6","description":""},{"key":"UserName","value":"sooriaprakkash","description":""},{"key":"JournyDate","value":"21-12-2019","description":""}]
}

If user trying to book with same kind information. API will throw alert.

http://localhost:3001/Login
http://localhost:3001/Dashboard/deleteAllUsers
http://localhost:3001/Dashboard/getRoles
```
