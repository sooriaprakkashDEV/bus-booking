const mongoose = require('mongoose');

mongoose
    .connect('mongodb://localhost/busbooking', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => console.log("Mongo DB Connected"))
    .catch(err => {
        console.log(`DB Connection Error: ${err.message}`);
    });