const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const busroutes = new Schema({
    SCity: { unique: false, type: String, trim: true, required: true },
    DCity: { unique: false, type: String, trim: true, required: true },
    DTime: { type: String, trim: true, required: true },
    ATime: { type: String, trim: true, required: true },
    Price: { type: String, trim: true, required: true },
    Duration: { type: String, trim: true, required: true },
    BusNumber: { unique: true, type: String, trim: true, required: true },
    OperatorName: { unique: false, type: String, trim: true, required: true },
    SortBy: { type: String, trim: true },
    SortOrder: { type: String, default: Date.now },
});

const ticketBooking = new Schema({
    BusNumber: { type: String, trim: true, required: true },
    SeatNumber: { type: String, trim: true, required: true },
    UserName: { type: String, trim: true, required: true },
    JournyDate: { type: String, trim: true, required: true },
});

// schema.set("toJSON", { virtuals: true });

module.exports = mongoose.model("busroute", busroutes);
module.exports = mongoose.model("ticketBooking", ticketBooking);
